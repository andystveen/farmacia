<?php

    class Proveedores extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Proveedor');

        }

        //Funcion que renderiza la vista index

        public function indexc(){

            $data['proveedores']=$this->Proveedor->obtenerTodos();
            $this->load->view('header');
            $this->load->view('proveedores/indexc',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoc(){
            $this->load->view('header');
            $this->load->view('proveedores/nuevoc');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosProveedor=array("cedula_pro"=>$this->input->post('cedula_pro'),"nombre_pro"=>$this->input->post('nombre_pro'),
          "apellido_pro"=>$this->input->post('apellido_pro'),"telefono_pro"=>$this->input->post('telefono_pro'),
          "nombre_empresa_pro"=>$this->input->post('nombre_empresa_pro'),"servicio_pro"=>$this->input->post('servicio_pro'),
          "direccion_pro"=>$this->input->post('direccion_pro')

        );
        if($this->Proveedor->insertar($datosNuevosProveedor)){
          redirect('proveedores/indexc');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_pro){
       if ($this->Proveedor->borrar($id_pro)){
         redirect('proveedores/indexc');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
