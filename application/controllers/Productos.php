<?php

    class Productos extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Producto');

        }

        //Funcion que renderiza la vista index

        public function indexb(){

            $data['productos']=$this->Producto->obtenerTodos();
            $this->load->view('header');
            $this->load->view('productos/indexb',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevob(){
            $this->load->view('header');
            $this->load->view('productos/nuevob');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosProducto=array("nombre_pro"=>$this->input->post('nombre_pro'),"precio_pro"=>$this->input->post('precio_pro'),
          "cantidad_pro"=>$this->input->post('cantidad_pro'),"fecha_elaboracion_pro"=>$this->input->post('fecha_elaboracion_pro'),
          "fecha_vencimiento_pro"=>$this->input->post('fecha_vencimiento_pro')
        );
        if($this->Producto->insertar($datosNuevosProducto)){
          redirect('productos/indexb');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_pro){
       if ($this->Producto->borrar($id_pro)){
         redirect('productos/indexb');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
