<?php

    class Clientes extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Cliente');

        }


        public function nosotros(){
          $this->load->view('header');
          $this->load->view('clientes/nosotros');
          $this->load->view('footer');
        }
        public function medicamentos(){
          $this->load->view('header');
          $this->load->view('clientes/medicamentos');
          $this->load->view('footer');
        }
        //Funcion que renderiza la vista index

        public function indexa(){

            $data['clientes']=$this->Cliente->obtenerTodos();
            $this->load->view('header');
            $this->load->view('clientes/indexa',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoa(){
            $this->load->view('header');
            $this->load->view('clientes/nuevoa');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosCliente=array("cedula_cli"=>$this->input->post('cedula_cli'),"nombre_cli"=>$this->input->post('nombre_cli'),
          "apellido_cli"=>$this->input->post('apellido_cli'),"telefono_cli"=>$this->input->post('telefono_cli'),
          "genero_cli"=>$this->input->post('genero_cli'),"correo_cli"=>$this->input->post('correo_cli'),
          "direccion_cli"=>$this->input->post('direccion_cli')
        );
        if($this->Cliente->insertar($datosNuevosCliente)){
          redirect('clientes/indexa');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_cli){
       if ($this->Cliente->borrar($id_cli)){
         redirect('clientes/indexa');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
