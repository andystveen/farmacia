<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">LISTADO DE PROVEEDORES</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url('proveedores/nuevoc'); ?>" class="btn btn-primary"> <i class="glyphicon glyphicon-plus"></i>  Agregar Proveedor</a>
  </div>
</div>
<div class="text-center">
  <img src="<?php echo base_url(); ?>/assets/images/proveedor.png" alt="" height="10%" width="10%">
</div>
<br>
<?php if ($proveedores): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>TELÉFONO</th>
        <th>NOMBRE EMPRESA</th>
        <th>SERVICIO</th>
        <th>DIRECCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($proveedores as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_empresa_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->servicio_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->direccion_pro; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Proveedor">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/proveedores/eliminar/<?php echo $filaTemporal->id_pro; ?>" onclick="return confirm('¿Estas seguro? de eliminar el registro seleccionado');" title="Eliminar Proveedor" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
