<h1 class="text-center">NUEVOS PROVEEDORES</h1>
<div class="text-center">
  <img src="<?php echo base_url(); ?>/assets/images/proveedor2.png" alt="" height="10%" width="10%">
</div>
<br>
<div class="container">


<form class="" action="<?php echo site_url(); ?>/proveedores/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_pro" value=""
          id="cedula_pro">

      </div>
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su nombre"
          class="form-control"
          name="nombre_pro" value=""
          id="nombre_pro">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese su apellido"
        class="form-control"
        name="apellido_pro" value=""
        id="apellido_pro">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Teléfono:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su telefono"
          class="form-control"
          name="telefono_pro" value=""
          id="telefono_pro">
      </div>
      <div class="col-md-4">
          <label for="">Nombre Empresa:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre de la empresa"
          class="form-control"
          name="nombre_empresa_pro" value=""
          id="nombre_empresa_pro">
      </div>
      <div class="col-md-4">
          <label for="">Servicio:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su servicio"
          class="form-control"
          name="servicio_pro" value=""
          id="servicio_pro">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_pro" value=""
          id="direccion_pro">
      </div>
    </div>
      <br>
      <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/proveedores/indexc"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</div>
</form>
