<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">LISTADO DE CLIENTES</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url('clientes/nuevoa'); ?>" class="btn btn-primary"> <i class="glyphicon glyphicon-plus"></i>  Agregar Cliente</a>
  </div>
</div>

<div class="text-center">
  <img src="<?php echo base_url(); ?>/assets/images/usuario.png" alt="" height="10%" width="10%">
</div>
<br>
<?php if ($clientes): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>CÉDULA</th>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        <th>TELÉFONO</th>
        <th>GÉNERO</th>
        <th>CORREO ELECTRÓNICO</th>
        <th>DIRECCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->genero_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->correo_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->direccion_cli; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Cliente">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>" onclick="return confirm('¿Estas seguro de eliminar el registro seleccionado?');" title="Eliminar Cliente" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
