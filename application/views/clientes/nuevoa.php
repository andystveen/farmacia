<h1 class="text-center">NUEVOS CLIENTES</h1>
<div class="text-center">
  <img src="<?php echo base_url(); ?>/assets/images/nuevo.png" alt="" height="10%" width="10%">
</div>
<br>
<div class="container">


<form class="" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_cli" value=""
          id="cedula_cli">

      </div>
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su nombre"
          class="form-control"
          name="nombre_cli" value=""
          id="nombre_cli">
      </div>
      <div class="col-md-4">
        <label for="">Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese su apellido"
        class="form-control"
        name="apellido_cli" value=""
        id="apellido_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Teléfono:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su telefono"
          class="form-control"
          name="telefono_cli" value=""
          id="telefono_cli">
      </div>
      <div class="col-md-4">
          <label for="">Género:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su genero"
          class="form-control"
          name="genero_cli" value=""
          id="genero_cli">
      </div>
      <div class="col-md-4">
          <label for="">Correo electrónico:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su correo"
          class="form-control"
          name="correo_cli" value=""
          id="correo_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_cli" value=""
          id="direccion_cli">
      </div>
    </div>
      <br>
      <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/indexa"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</div>
</form>
