<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/aspirina.png" alt="..." height="1300px" width="1300px">
      <div class="caption">
        <h3 class="text-center">Aspirina</h3>
        <p class="text-center">Precio: $6,00</p>
        <p class="text-center"><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-danger" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/omeprazol.png" alt="..." height="320px" width="320px">
      <div class="caption">
        <h3 class="text-center">Omeprazol</h3>
        <p class="text-center">Precio: $5,50</p>
        <p class="text-center"><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-danger" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/paracetamol.png" alt="..." height="300px" width="300px">
      <div class="caption">
        <h3 class="text-center">Paracetamol</h3>
        <p class="text-center">Precio: $4,00</p>
        <p class="text-center"><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-danger" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/lanzoprazol.png" alt="..." height="1400px" width="1400px">
      <div class="caption">
        <h3 class="text-center">Lansoprazol</h3>
        <p class="text-center">Precio: $5.99</p>
        <p class="text-center"><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-danger" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/salbutamol.png" alt="...">
      <div class="caption">
        <h3 class="text-center">Salbutamol</h3>
        <p class="text-center">Precio: $5,25</p>
        <p class="text-center"><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-danger" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/images/ramipril.png" alt="...">
      <div class="caption">
        <h3 class="text-center">Ramipril</h3>
        <p class="text-center">Precio: $5,80</p>
        <p class="text-center"><a href="#" class="btn btn-primary" role="button">Comprar</a> <a href="#" class="btn btn-danger" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
</div>
