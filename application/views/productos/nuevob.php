<h1 class="text-center">NUEVOS PRODUCTOS</h1>
<div class="text-center">
  <img src="<?php echo base_url(); ?>/assets/images/medic1.png" alt="" height="8%" width="8%">
</div>
<br>
<div class="container">


<form class="" action="<?php echo site_url(); ?>/productos/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del producto"
          class="form-control"
          name="nombre_pro" value=""
          id="nombre_pro">

      </div>
      <div class="col-md-4">
          <label for="">Precio:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su precio"
          class="form-control"
          name="precio_pro" value=""
          id="precio_pro">
      </div>
      <div class="col-md-4">
        <label for="">Cantidad:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la cantidad"
        class="form-control"
        name="cantidad_pro" value=""
        id="cantidad_pro">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Fecha Elaboración:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la fecha de elaboracion"
          class="form-control"
          name="fecha_elaboracion_pro" value=""
          id="fecha_elaboracion_pro">
      </div>
      <div class="col-md-4">
          <label for="">Fecha Vencimiento:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la fecha de vencimiento"
          class="form-control"
          name="fecha_vencimiento_pro" value=""
          id="fecha_vencimiento_pro">
      </div>
    </div>
      <br>
      <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/productos/indexb"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</div>
</form>
