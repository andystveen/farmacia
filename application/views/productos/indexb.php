<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">LISTADO DE PRODUCTOS</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url('productos/nuevob'); ?>" class="btn btn-primary"> <i class="glyphicon glyphicon-plus"></i>  Agregar Producto</a>
  </div>
</div>
<div class="text-center">
  <img src="<?php echo base_url(); ?>/assets/images/medic.png" alt="" height="10%" width="10%">
</div>
<br>
<?php if ($productos): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>PRECIO</th>
        <th>CANTIDAD</th>
        <th>FECHA ELABORACIÓN</th>
        <th>FECHA VENCIMIENTO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($productos as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->precio_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->cantidad_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->fecha_elaboracion_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->fecha_vencimiento_pro; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Producto">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/productos/eliminar/<?php echo $filaTemporal->id_pro; ?>" onclick="return confirm('¿Estas seguro de eliminar el registro seleccionado?');" title="Eliminar Producto" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
